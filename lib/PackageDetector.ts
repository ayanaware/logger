import * as fs from 'fs';
import * as path from 'path';

/**
 * @ignore
 */
export class PackageDetector {
	public readonly packageCache: Map<string, any> = new Map();

	public getRootOf(directory: string) {
		// Find the next package.json file in the directory tree
		let files = this.readdirSync(directory);
		while (!files.includes('package.json')) {
			const up = path.join(directory, '..');

			// If there is no package.json we will get stuck at the root directory
			if (up === directory) {
				directory = null;
				break;
			}

			directory = up;
			files = this.readdirSync(directory);
		}

		if (directory == null) throw new Error('No package.json could be found in the directory tree');

		return directory;
	}

	public getInfo(rootDir: string) {
		// Get package.json location
		const packageFile = path.resolve(rootDir, 'package.json');

		// Load package.json from cache or require it
		let pkg;
		if (this.packageCache.get(packageFile) == null) {
			pkg = JSON.parse(this.readFileSync(packageFile, { encoding: 'utf8' }));
			this.packageCache.set(packageFile, pkg);
		} else {
			pkg = this.packageCache.get(packageFile);
		}

		return pkg;
	}

	// Credit: https://github.com/stefanpenner/get-caller-file/blob/master/index.ts
	public getCallerFile(position = 2) {
		if (position >= Error.stackTraceLimit) {
			throw new TypeError('getCallerFile(position) requires position be less then Error.stackTraceLimit but position was: `' + position + '` and Error.stackTraceLimit was: `' + Error.stackTraceLimit + '`');
		}

		const oldPrepareStackTrace = Error.prepareStackTrace;
		Error.prepareStackTrace = (_, stack)  => stack;
		const stack: any = new Error().stack;
		Error.prepareStackTrace = oldPrepareStackTrace;

		if (stack === null || typeof stack !== 'object') return null;
		  
		// stack[0] holds this file
		// stack[1] holds where this function was called
		// stack[2] holds the file we're interested in
		return stack[position] ? (stack[position] as any).getFileName() : undefined;
	}

	// Wrapper functions so we can test this without context

	/* istanbul ignore next */
	public readdirSync(dirPath: fs.PathLike) {
		return fs.readdirSync(dirPath);
	}

	/* istanbul ignore next */
	public readFileSync(filePath: fs.PathLike, options: { encoding: 'utf8' }): string {
		return fs.readFileSync(filePath, options);
	}
}
