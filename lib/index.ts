import { Logger } from './Logger';

/**
 * Alias for {@link Logger.get}.
 *
 * @see {@link Logger.get}
 */
const get = Logger.get;
export { Logger, get };
export default Logger;

export * from './LogMeta';
export * from './PackageDetector';

export * from './constants/LogLevel';
export * from './constants/LogLevelValue';

export * from './formatter/Color';
export * from './formatter/Formatter';
export * from './formatter/default/DefaultFormatter';
export * from './formatter/default/DefaultFormatterColor';

export * from './transports/Transport';
export * from './transports/ConsoleTransport';

export * from './types/LogLine';
