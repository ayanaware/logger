'use strict';

const { Logger } = require('../../../build/Logger');

describe('#constructor', function() {
	it('should define name, package name and package path', function() {
		const extra = {};
		const logger = new Logger('TestName', 'TestPackageName', 'TestPackagePath', extra);

		expect(logger.name, 'to be', 'TestName');
		expect(logger.packageName, 'to be', 'TestPackageName');
		expect(logger.packagePath, 'to be', 'TestPackagePath');
		expect(logger.extra, 'to be', extra);
	});
});
